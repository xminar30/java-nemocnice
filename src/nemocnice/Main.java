package nemocnice;

import java.util.Scanner;

import pacienti.*;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Databaze db = new Databaze();
		Scanner sc = new Scanner(System.in);
		Pacient p = null;
		
		// pocatecni vypsani napovedy
		vypisMoznosti();
		
		// hlavni smycka programu, prijima jednotlive prikazy a reaguje na ne
		boolean konec = false;
		while (!konec) {
			System.out.print("> ");
			switch (sc.nextLine()) {
			case "pridej":
			case "1":
				
				Skupina s = null;
				while (true) {
					System.out.println("Zadejte skupinu A, B nebo C");
					switch (sc.nextLine()) {
					case "A":
					case "a":
						s = Skupina.SKUPINA_A;
						break;
					case "B":
					case "b":
						s = Skupina.SKUPINA_B;
						break;
					case "C":
					case "c":
						s = Skupina.SKUPINA_C;
						break;
					}
					if (s != null) break;
				}
				System.out.println("Zadejte jmeno, prijmeni, rok narozeni a mnozstvi latky/latek");
				System.out.println("vse oddelene mezerami");
				String line = sc.nextLine();
				String[] pac = line.split(" ");
				p = null;
				try {
					switch (s) {
					case SKUPINA_A:
						p = new PacientA(
								pac[0],
								pac[1],
								Integer.parseInt(pac[2]),
								Double.parseDouble(pac[3]));
						break;
					case SKUPINA_B:
						p = new PacientB(
								pac[0],
								pac[1],
								Integer.parseInt(pac[2]),
								Double.parseDouble(pac[3]));
						break;
					case SKUPINA_C:
						p = new PacientC(
								pac[0],
								pac[1],
								Integer.parseInt(pac[2]),
								Double.parseDouble(pac[3]),
								Double.parseDouble(pac[4]));
						break;
					}
					db.add(p);
					System.out.println("pacient pridan pod id " + p.getId());
				} catch (Exception e) {
					System.out.println("chybne zadany vstup");
				}
				break;
				
			case "zmen":
			case "2":
				System.out.println("Zadejte id pacienta");
				int id = sc.nextInt();
				try {
					p = db.get(id);
					System.out.println("Zadejte nove mnozstvi leku");
					switch (p.getSkupina()) {
					case SKUPINA_A:
						((PacientA)p).setMnozstviLatky(sc.nextDouble());
						break;
					case SKUPINA_B:
						((PacientB)p).setMnozstviLatky(sc.nextDouble());
						break;
					case SKUPINA_C:
						((PacientC)p).setMnozstviLatky(sc.nextDouble());
						System.out.println("Zadejte nove mnozstvi podpurne latky C");
						((PacientC)p).setMnozstviPodpurneLatkyC(sc.nextDouble());
						break;
					}
					System.out.println("ulozeno");
				} catch (Exception e) {
					System.out.println("Pacient nenalezen");
				}
				break;
			case "propust":
			case "3":
				System.out.println("Zadejte id pacienta");
				db.remove(sc.nextInt());
				System.out.println("pacient propusten");
				break;
			case "najdi":
			case "4":
				System.out.println("Zadejte id pacienta");
				try {
					db.get(sc.nextInt()).vypis();
				} catch (Exception e) {
					System.out.println("Pacient nenalezen");
				}
				System.out.println();
				break;
			case "vypis":
			case "5":
				db.vypisPacientyAbecedne();
				break;
			case "leky":
			case "6":
				db.vypisPotrebneLeky();
				break;
			case "uloz":
			case "7":
				System.out.println("Zadejte nazev souboru pro ulozeni");
				String fw = sc.nextLine();
				db.uloz(fw);
				System.out.println("Databaze ulozena do souboru " + fw);
				break;
			case "nacti":
			case "8":
				System.out.println("Zadejte nazev souboru pro nacteni");
				String fr = sc.nextLine();
				db.nahraj(fr);
				System.out.println("Databaze nactena ze souboru " + fr);
				break;
			case "napoveda":
			case "9":
				vypisMoznosti();
				break;
			case "konec":
			case "10":
				konec = true;
				break;
			}
		}
		sc.close();
	}
	
	// vypis napovedy
	private static void vypisMoznosti() {
		System.out.println("Moznosti:");
		System.out.println("1) pridej - prida noveho pacienta");
		System.out.println("2) zmen - zmeni mnozstvi latky/latek");
		System.out.println("3) propust - propusti pacienta");
		System.out.println("4) najdi - nalezne pacienta a vypise informace");
		System.out.println("5) vypis - vypise abecedne razeny seznam pacientu");
		System.out.println("6) leky - vypise potrebne mnozstvi leku na den");
		System.out.println("7) uloz - ulozeni databaze do souboru");
		System.out.println("8) nacti - nacteni databaze ze souboru");
		System.out.println("9) napoveda - vypise tyto moznosti");
		System.out.println("10) konec - ukonci program");
	}

}