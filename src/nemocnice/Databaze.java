package nemocnice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import pacienti.*;

// trida predstavujici databazi
public class Databaze {
	private HashMap<Integer, Pacient> databaze;
	private LinkedList<Integer> idAbecedneA;
	private LinkedList<Integer> idAbecedneB;
	private LinkedList<Integer> idAbecedneC;
	
	// ulozi databazi do souboru
	public void uloz(String soubor) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(soubor)));
			this.zapisSeznam(this.idAbecedneA, bw);
			bw.write("KONEC");
			bw.newLine();
			
			this.zapisSeznam(this.idAbecedneB, bw);
			bw.write("KONEC");
			bw.newLine();
			
			this.zapisSeznam(this.idAbecedneC, bw);
			bw.write("KONEC");
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			System.err.println("Chyba pri zapisu souboru " + soubor + ".");
		}
	}
	
	// zapise do souboru seznam pacientu jedne ze skupin
	private void zapisSeznam(LinkedList<Integer> l, BufferedWriter bw) throws IOException {
		for (Iterator<Integer> it = l.iterator(); it.hasNext();) {
			int i = (int) it.next();
			Pacient p = this.databaze.get(i);
			bw.write(p.getId()+" ");
			bw.write(p.getJmeno()+" ");
			bw.write(p.getPrijmeni()+" ");
			bw.write(p.getRokNarozeni()+" ");
			bw.write(""+p.getMnozstviLatky());
			if (p.getSkupina() == Skupina.SKUPINA_C) {
				PacientC pc = (PacientC)p;
				bw.write(" "+pc.getMnozstviPodpurneLatkyC());
			}
			bw.newLine();
		}
	}
	
	// nacte databazi ze souboru
	public void nahraj(String soubor) {
		this.databaze.clear();
		this.idAbecedneA.clear();
		this.idAbecedneB.clear();
		this.idAbecedneC.clear();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(soubor)));
			nahrajSeznam(this.idAbecedneA, br, Skupina.SKUPINA_A);
			nahrajSeznam(this.idAbecedneB, br, Skupina.SKUPINA_B);
			nahrajSeznam(this.idAbecedneC, br, Skupina.SKUPINA_C);
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("Soubor " + soubor + " se nepodarilo otevrit.");
		} catch (IOException e) {
			System.err.println("Chyba pri cteni souboru " + soubor + ".");
		}
	}
	
	// nacte ze souboru seznam pacientu jedne ze skupin
	private void nahrajSeznam(LinkedList<Integer> l, BufferedReader br, Skupina s) throws IOException {
		int max_id = 0;
		String line;
		while ((line = br.readLine()) != null) {
			if (line.compareTo("KONEC") == 0) break;
			String[] pacient = line.split(" ");
			Pacient p = null;
			switch (s) {
			case SKUPINA_A:
				p = new PacientA(
						Integer.parseInt(pacient[0]),
						pacient[1],
						pacient[2],
						Integer.parseInt(pacient[3]),
						Double.parseDouble(pacient[4]));
				this.idAbecedneA.add(p.getId());
				break;
			case SKUPINA_B:
				p = new PacientB(
						Integer.parseInt(pacient[0]),
						pacient[1],
						pacient[2],
						Integer.parseInt(pacient[3]),
						Double.parseDouble(pacient[4]));
				this.idAbecedneB.add(p.getId());
				break;
			case SKUPINA_C:
				p = new PacientC(
						Integer.parseInt(pacient[0]),
						pacient[1],
						pacient[2],
						Integer.parseInt(pacient[3]),
						Double.parseDouble(pacient[4]),
						Double.parseDouble(pacient[5]));
				this.idAbecedneC.add(p.getId());
				break;
			}
			if (p.getId() > max_id)
				max_id = p.getId();
			this.databaze.put(p.getId(), p);
		}
		if (Pacient.getLast_id() < max_id) {
			Pacient.setLast_id(max_id);
		}
	}
	
	// vrati pacienta podle id
	public Pacient get(int id) {
		return this.databaze.get(id);
	}
	
	// odebere pacienta z databaze podle id
	public void remove(int id) {
		Pacient p = this.databaze.get(id);
		switch (p.getSkupina()) {
		case SKUPINA_A:
			((PacientA)p).setMnozstviLatky(0);
			break;
		case SKUPINA_B:
			((PacientB)p).setMnozstviLatky(0);
		case SKUPINA_C:
			((PacientC)p).setMnozstviPodpurneLatkyC(0);
			break;
		}
		this.databaze.remove(id);
		idAbecedneA.remove((Integer)id);
		idAbecedneB.remove((Integer)id);
		idAbecedneC.remove((Integer)id);
	}
	
	// prida pacienta
	public void add(Pacient p) {
		databaze.put(p.getId(), p);
		switch (p.getSkupina()) {
		case SKUPINA_A:
			this.pridejAbecedne(p, this.idAbecedneA);
			break;
		case SKUPINA_B:
			this.pridejAbecedne(p, this.idAbecedneB);
			break;
		case SKUPINA_C:
			this.pridejAbecedne(p, this.idAbecedneC);
			break;
		}
	}
	
	// zaradi pacienta do seznamu pro moznost abecedniho vypisu
	private void pridejAbecedne(Pacient p, LinkedList<Integer> l) {
		int index = 0;
		for (Iterator<Integer> it = l.iterator(); it.hasNext();) {
			Integer i = (Integer) it.next();
			if (p.getPrijmeni().compareTo(this.databaze.get(i).getPrijmeni()) < 0) {
				break;
			}
			index++;
		}
		l.add(index, p.getId());
	}
	
	// vypise abecedne pacienty v jednotlivych skupinach
	public void vypisPacientyAbecedne() {
		System.out.println("Skupina A");
		vypisSeznam(this.idAbecedneA);
		System.out.println("Skupina B");
		vypisSeznam(this.idAbecedneB);
		System.out.println("Skupina C");
		vypisSeznam(this.idAbecedneC);
	}
	
	// vypise jeden ze seznamu pacientu
	private void vypisSeznam(LinkedList<Integer> l) {
		for (Iterator<Integer> it = l.iterator(); it.hasNext();) {
			Integer i = (Integer) it.next();
			Pacient p = databaze.get(i);
			p.vypis();
			System.out.println();
		}
	}
	
	// vypise celkove mnozstvi potrebnych leku
	public void vypisPotrebneLeky() {
		System.out.println("Potrebne mnozstvi leku");
		System.out.println("Lek A: " + PacientA.getCelkoveMnozstviLatkyA());
		System.out.println("Lek B: " + PacientB.getCelkoveMnozstviLatkyB());
		System.out.println("Lek C: " + PacientC.getCelkoveMnozstviLatkyC());
	}
	
	// konstruktor
	public Databaze() {
		this.databaze = new HashMap<Integer, Pacient>();
		this.idAbecedneA = new LinkedList<Integer>();
		this.idAbecedneB = new LinkedList<Integer>();
		this.idAbecedneC = new LinkedList<Integer>();
	}
}
