package pacienti;

import pacienti.Skupina;

//trida pro pacienta ze skupiny B, dedena ze tridy Pacient
public class PacientB extends Pacient {
	protected static double celkoveMnozstviLatkyB = 0;

	public static double getCelkoveMnozstviLatkyB() {
		return celkoveMnozstviLatkyB;
	}

	public static void setCelkoveMnozstviLatkyB(double celkoveMnozstviLatkyB) {
		PacientB.celkoveMnozstviLatkyB = celkoveMnozstviLatkyB;
	}

	public void setMnozstviLatky(double mnozstviLatky) {
		PacientB.celkoveMnozstviLatkyB -= this.mnozstviLatky;
		PacientB.celkoveMnozstviLatkyB += mnozstviLatky;
		this.mnozstviLatky = mnozstviLatky;
	}

	public PacientB(String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatky) {
		super();
		this.skupina = Skupina.SKUPINA_B;
		this.jmeno = jmeno;
		this.prijmeni = prijmeni;
		this.rokNarozeni = rokNarozeni;
		this.setMnozstviLatky(mnozstviLatky);
	}
	
	public PacientB(int id, String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatky) {
		this.id = id;
		this.skupina = Skupina.SKUPINA_B;
		this.jmeno = jmeno;
		this.prijmeni = prijmeni;
		this.rokNarozeni = rokNarozeni;
		this.setMnozstviLatky(mnozstviLatky);
	}
	
	public void vypis() {
		super.vypis();
		System.out.print("Latka B: " + this.getMnozstviLatky());
	}
}
