package pacienti;

//trida pro pacienta ze skupiny C, dedena ze tridy PacientB
public class PacientC extends PacientB {
	private static double celkoveMnozstviLatkyC = 0;
	private double mnozstviPodpurneLatkyC;
	
	public static double getCelkoveMnozstviLatkyC() {
		return celkoveMnozstviLatkyC;
	}

	public static void setCelkoveMnozstviLatkyC(double celkoveMnozstviLatkyC) {
		PacientC.celkoveMnozstviLatkyC = celkoveMnozstviLatkyC;
	}

	public PacientC(String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatkyB, double mnozstviPodpurneLatkyC) {
		super(jmeno, prijmeni, rokNarozeni, mnozstviLatkyB);
		this.skupina = Skupina.SKUPINA_C;
		this.setMnozstviPodpurneLatkyC(mnozstviPodpurneLatkyC);
	}
	
	public PacientC(int id, String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatkyB, double mnozstviPodpurneLatkyC) {
		super(id, jmeno, prijmeni, rokNarozeni, mnozstviLatkyB);
		this.skupina = Skupina.SKUPINA_C;
		this.setMnozstviPodpurneLatkyC(mnozstviPodpurneLatkyC);
	}

	public double getMnozstviPodpurneLatkyC() {
		return mnozstviPodpurneLatkyC;
	}

	public void setMnozstviPodpurneLatkyC(double mnozstviPodpurneLatkyC) {
		PacientC.celkoveMnozstviLatkyC -= this.mnozstviPodpurneLatkyC;
		PacientC.celkoveMnozstviLatkyC += mnozstviPodpurneLatkyC;
		this.mnozstviPodpurneLatkyC = mnozstviPodpurneLatkyC;
	}
	
	public void vypis() {
		super.vypis();
		System.out.print(", Latka C: "
		+ this.getMnozstviPodpurneLatkyC());
	}
	
}
