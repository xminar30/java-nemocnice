package pacienti;

import pacienti.Skupina;

// trida pro pacienta ze skupiny A, dedena ze tridy Pacient
public class PacientA extends Pacient {
	private static double celkoveMnozstviLatkyA = 0;

	public static double getCelkoveMnozstviLatkyA() {
		return celkoveMnozstviLatkyA;
	}

	public static void setCelkoveMnozstviLatkyA(double celkoveMnozstviLatkyA) {
		PacientA.celkoveMnozstviLatkyA = celkoveMnozstviLatkyA;
	}

	public void setMnozstviLatky(double mnozstviLatky) {
		PacientA.celkoveMnozstviLatkyA -= this.mnozstviLatky;
		PacientA.celkoveMnozstviLatkyA += mnozstviLatky;
		this.mnozstviLatky = mnozstviLatky;
	}

	public PacientA(String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatky) {
		super();
		this.skupina = Skupina.SKUPINA_A;
		this.jmeno = jmeno;
		this.prijmeni = prijmeni;
		this.rokNarozeni = rokNarozeni;
		this.setMnozstviLatky(mnozstviLatky);
	}
	
	public PacientA(int id, String jmeno, String prijmeni, int rokNarozeni,
			double mnozstviLatky) {
		this.id = id;
		this.skupina = Skupina.SKUPINA_A;
		this.jmeno = jmeno;
		this.prijmeni = prijmeni;
		this.rokNarozeni = rokNarozeni;
		this.setMnozstviLatky(mnozstviLatky);
	}
	
	public void vypis() {
		super.vypis();
		System.out.print("Latka A: " + this.getMnozstviLatky());
	}
}
