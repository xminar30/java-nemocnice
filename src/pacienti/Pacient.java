package pacienti;

// trida pro obecneho pacienta, vyuzita pro dedeni konkretnich pacientu ve skupinach
public abstract class Pacient {
	private static int last_id = 0;
	protected int id;
	protected String jmeno;
	protected String prijmeni;
	protected int rokNarozeni;
	protected Skupina skupina;
	protected double mnozstviLatky;

	public static int getLast_id() {
		return last_id;
	}
	public static void setLast_id(int last_id) {
		Pacient.last_id = last_id;
	}
	public int getId() {
		return id;
	}
	public String getJmeno() {
		return jmeno;
	}
	public String getPrijmeni() {
		return prijmeni;
	}
	public int getRokNarozeni() {
		return rokNarozeni;
	}
	public Skupina getSkupina() {
		return skupina;
	}
	public double getMnozstviLatky() {
		return mnozstviLatky;
	}
	
	// vypise udaje o pacientovi
	public void vypis() {
		System.out.print(this.getId() + " "
				+ this.getJmeno() + " "
				+ this.getPrijmeni() + " "
				+ this.getRokNarozeni() + " ");
	}
	
	// konstruktor, automaticky zvysi id
	public Pacient() {
		last_id += 1;
		this.id = last_id;
	}
	
}
